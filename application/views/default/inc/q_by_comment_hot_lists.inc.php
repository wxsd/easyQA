<h3 class="page-title">近期热议</h3>
<ol class="fly-list-one">
    <?php if (is_array($q_by_comment_hot_lists)): ?>
        <?php foreach ($q_by_comment_hot_lists as $_q): ?>
            <li>
                <a href="/q/detail/<?=$_q['id']?>"><?=xss_filter($_q['article_title'])?></a>
                <span><?=$_q['comment_counts']?> <i class="iconfont">&#xe60c;</i></span>
            </li>
        <?php endforeach;?>
    <?php endif;?>
</ol>